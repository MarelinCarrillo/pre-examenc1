package com.example.pre_examen;

import java.io.Serializable;
import java.util.Random;


public class ReciboNomina implements Serializable {
    private int numRecibo, puesto;
    private String Nombre;
    private float horasTrabNormal, horasTrabExtra;
    private float impuesto;

    public ReciboNomina(int numRecibo, int puesto, String nombre, float horasTrabNormal, float horasTrabExtra, float impuesto) {
        this.numRecibo = numRecibo;
        this.puesto = puesto;
        Nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtra = horasTrabExtra;
        this.impuesto = impuesto;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtra() {
        return horasTrabExtra;
    }

    public void setHorasTrabExtra(float horasTrabExtra) {
        this.horasTrabExtra = horasTrabExtra;
    }

    public float getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(float impuesto) {
        this.impuesto = impuesto;
    }

    public int numFolio(){
        Random r = new Random();
        return r.nextInt(1000) + 1;
    }

    public float calcularSubtotal(){
        float pagoInicial = 200;
        float pagoBase = pagoInicial;

        if (puesto == 1) {
            pagoBase *= 1.20; // 20% increment
        } else if (puesto == 2) {
            pagoBase *= 1.50; // 50% increment
        } else if (puesto == 3) {
            pagoBase *= 2.00; // 100% increment
        }

        return (pagoBase * horasTrabNormal) + (pagoBase * horasTrabExtra * 2);
    }

    public float calcularImpuesto(){
        this.impuesto = 0.16f;
        return calcularSubtotal() * this.impuesto;
    }

    public float calcularTotal() {
        return calcularSubtotal() - calcularImpuesto();
    }

}