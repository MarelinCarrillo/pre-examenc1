package com.example.pre_examen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import java.util.Random;


public class ReciboNominaActivity extends AppCompatActivity {
    private ReciboNomina recibo;
    private TextView lblNumRecibo, lblNombre, lblSubtotal, lblImpuesto, lblTotal;
    private EditText txtHorasNormal, txtHorasExtras;
    private RadioButton rdbAuxiliar, rdbAlbañil, rdbIngObra;
    private Button btnCalcular, btnLimpiar, btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_recibo_nomina);

        iniciarComponentes();

        // Obtener el nombre pasado desde MainActivity
        String nombre = getIntent().getStringExtra("cliente");
        lblNombre.setText("Nombre:" + nombre);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcular();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblNumRecibo.setText("Numero de Recibo: ");
                txtHorasNormal.setText("");
                txtHorasExtras.setText("");
                rdbAuxiliar.setChecked(true);
                lblSubtotal.setText("Subtotal: ");
                lblImpuesto.setText("Impuesto: ");
                lblTotal.setText("Total: ");
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes() {
        lblNumRecibo = (TextView) findViewById(R.id.lblNumRecibo);
        lblNombre = (TextView) findViewById(R.id.lblNombre);

        txtHorasNormal = (EditText) findViewById(R.id.txtHorasNormal);
        txtHorasExtras = (EditText) findViewById(R.id.txtHorasExtras);

        rdbAuxiliar = (RadioButton) findViewById(R.id.rdbAuxiliar);
        rdbAlbañil = (RadioButton) findViewById(R.id.rdbAlbañil);
        rdbIngObra = (RadioButton) findViewById(R.id.rdbIngObra);

        lblSubtotal = (TextView) findViewById(R.id.lblSubtotal);
        lblImpuesto = (TextView) findViewById(R.id.lblImpuesto);
        lblTotal = (TextView) findViewById(R.id.lblTotal);

        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnSalir = (Button) findViewById(R.id.btnSalir);
    }

    private void calcular() {
        String nombre = lblNombre.getText().toString().replace("Nombre: ", "");
        if (txtHorasNormal.getText().toString().isEmpty() || txtHorasExtras.getText().toString().isEmpty()) {
            Toast.makeText(this, "Por favor, ingrese las horas trabajadas y horas extras.", Toast.LENGTH_SHORT).show();
            return;
        }
        float horasNormales = Float.parseFloat(txtHorasNormal.getText().toString());
        float horasExtras = Float.parseFloat(txtHorasExtras.getText().toString());
        int puesto = 1; // Default puesto

        if (rdbAuxiliar.isChecked()) {
            puesto = 1;
        } else if (rdbAlbañil.isChecked()) {
            puesto = 2;
        } else if (rdbIngObra.isChecked()) {
            puesto = 3;
        }

        int numRecibo = new Random().nextInt(1000); // Generar un número de recibo aleatorio

        recibo = new ReciboNomina(numRecibo, puesto, nombre, horasNormales, horasExtras, 0);

        float subtotal = recibo.calcularSubtotal();
        float impuesto = recibo.calcularImpuesto();
        float total = recibo.calcularTotal();

        lblNumRecibo.setText("Numero de Recibo: " + recibo.getNumRecibo());
        lblSubtotal.setText("Subtotal: " + subtotal);
        lblImpuesto.setText("Impuesto: " + impuesto);
        lblTotal.setText("Total: " + total);
    }


}
